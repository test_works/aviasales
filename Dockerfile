FROM ruby:2.5.5

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN bundle install -j "$(getconf _NPROCESSORS_ONLN)" --retry 5

COPY . ./

EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]
