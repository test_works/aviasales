# README

* Ruby version - **2.5.5p157**

* Rails version - **5.2.4.1**

* Bundler version - **1.17.3**

***
* docker-compose build \\  
&& docker-compose run rails-api rake db:drop db:create db:migrate db:seed \\  
&& docker-compose up
***

-[x] rubocop

-[x] rspec

-[x] brakeman

***
Для метода **subscribe** необходимо создать пользователя (user). По роуту POST /sessions
передать в теле запроса 2 параметра "email" and "password", в ответе получить токен. Полученный токен 
использовать для авторизации в формате "Bearer token"

Для бана аналогично. Создать админа (admin). POST /admin/sessions.

user creation - FactoryBot.create(:user)  
admin creation - FactoryBot.create(:admin)

Пароли для фабрики - develop
