class CreateUsersProgramsInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :users_programs_infos do |t|
      t.belongs_to :user
      t.belongs_to :program
      t.boolean :ban, default: false

      t.timestamps
    end
  end
end
