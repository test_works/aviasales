class User < ApplicationRecord
  include Authenticatable

  # Attributes
  has_secure_password
  attr_accessor :token

  # Associations
  has_many :users_programs_infos, dependent: :destroy, counter_cache: true
  has_many :programs, through: :users_programs_infos, counter_cache: true
  has_many :available_programs, ->{ where('users_programs_infos.ban = ?', false) }, through: :users_programs_infos, source: :program

  # Callbacks
  before_validation :normalize_name

  # Validations
  validates :password_digest, presence: true
  validates :name, presence: true
  validates :email, presence: true, format: { with: /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/ }, length: { minimum: 1 }, uniqueness: true

  private

  def normalize_name
    self.name = name&.split&.map(&:capitalize)&.join(' ')
  end
end
