class Program < ApplicationRecord
  # Associations
  has_many :users_programs_infos, dependent: :destroy, counter_cache: true
  has_many :users, through: :users_programs_infos, counter_cache: true

  # Callbacks
  before_validation :normalize_title
  before_validation :normalize_description

  # Scopes
  scope :term, ->(term) { where('programs.title LIKE ?', "%#{term}%") }
  scope :by_popularity, -> do
    left_joins(:users_programs_infos)
      .group(:id)
      .order('COUNT(users_programs_infos.id) DESC')
  end

  # Validations
  validates :title, presence: true, length: { minimum: 1, maximum: 30 }
  validates :description, presence: true, length: { minimum: 1, maximum: 140 }

  private

  def normalize_title
    self.title = title&.capitalize
  end

  def normalize_description
    self.description = description&.capitalize
  end
end
