class UsersProgramsInfo < ApplicationRecord
  # Associations
  belongs_to :user
  belongs_to :program
end
