class Admin < ApplicationRecord
  include Authenticatable

  # Attributes
  has_secure_password
  attr_accessor :token

  # Validations
  validates :password_digest, presence: true
  validates :email, presence: true, format: { with: /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/ }, length: { minimum: 1 }, uniqueness: true
end
