class UsersController < ApplicationController
  before_action :set_user, only: [:show]

  def show
    render json: UserSerializer.new(@user).serialized_json
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: UserSerializer.new(@user).serialized_json, status: :created
    else
      respond_with_errors(@user)
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.permit(*whitelisted)
  end

  def whitelisted
    [
      :email,
      :name,
      :password
    ]
  end
end
