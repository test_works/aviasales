module Admins
  class ProgramsController < AdminController
    before_action :set_program, only: [:ban]

    def ban
      return render json: { message: 'User email required' }, status: :unprocessable_entity unless params[:email]

      @user = User.find_by(email: params[:email])
      return render_not_found_response unless @user

      if @program.users.include?(@user)
        @program.users_programs_infos.find_by(user: @user).update(ban: true)
        render json: { message: :banned }, status: :ok
      else
        render json: { message: 'User is not a subscriber' }, status: :ok
      end
    end

    private

    def set_program
      @program = Program.find(params[:id])
    end
  end
end
