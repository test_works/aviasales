module Admins
  class AdminTokenController < Knock::AuthTokenController
    skip_forgery_protection

    def create
      @token = auth_token
      @admin = entity
      render json: Admins::AdminSerializer.new(@admin, params: { token: @token.token }).serialized_json
    end

    private

    def entity
      @entity ||=
        if entity_class.respond_to?(:from_token_request)
          entity_class.from_token_request(request)
        else
          entity_class.find_by(email: auth_params[:email])
        end
    end

    def auth_params
      params.permit([:email, :password])
    end
  end
end
