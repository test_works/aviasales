module Users
  class ProgramsController < ApplicationController
    before_action :set_user, only: [:index]

    def index
      @programs = @user.available_programs
      render json: ProgramSerializer.new(@programs).serialized_json
    end

    private

    def set_user
      @user = User.find(params[:user_id])
    end
  end
end
