class ProgramsController < ApplicationController
  before_action :set_program, only: [:subscribe]
  before_action :authenticate_user, only: [:subscribe]

  has_scope :term, only: [:popular]

  def index
    @programs = Program.all
    render json: ProgramSerializer.new(@programs).serialized_json
  end

  def subscribe
    UsersProgramsInfo.find_or_create_by(program: @program, user: current_user)
    render json: { message: :subscribed }, status: :ok
  end

  def popular
    @programs = apply_scopes(Program.by_popularity)
    render json: ProgramSerializer.new(@programs).serialized_json
  end

  private

  def set_program
    @program = Program.find(params[:id])
  end
end
