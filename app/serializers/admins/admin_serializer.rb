module Admins
  class AdminSerializer
    include FastJsonapi::ObjectSerializer
    set_key_transform :camel_lower

    attributes :email

    attribute :token do |_drop_down_value, params|
      params[:token]
    end
  end
end
