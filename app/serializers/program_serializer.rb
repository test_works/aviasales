class ProgramSerializer
  include FastJsonapi::ObjectSerializer
  set_key_transform :camel_lower

  attributes :title,
             :description
end
