class UserSerializer
  include FastJsonapi::ObjectSerializer
  set_key_transform :camel_lower

  attributes :email,
             :name

  attribute :token do |_drop_down_value, params|
    params[:token]
  end
end
