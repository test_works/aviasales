Rails.application.routes.draw do
  # --------------------------------------------------------------------------------------------------------------------
  # Authorization
  # --------------------------------------------------------------------------------------------------------------------
  post 'sessions' => 'user_token#create', defaults: { format: :json }
  post 'admins/sessions' => 'admins/admin_token#create', defaults: { format: :json }

  # --------------------------------------------------------------------------------------------------------------------
  # Admin
  # --------------------------------------------------------------------------------------------------------------------
  namespace :admins, defaults: { format: :json } do
    resources :programs, only: [], defaults: { format: :json } do
      member do
        put :ban, to: 'programs#ban'
      end
    end
  end

  # --------------------------------------------------------------------------------------------------------------------
  # Users
  # --------------------------------------------------------------------------------------------------------------------
  resources :users, only: [:show, :create], defaults: { format: :json } do
    scope module: :users do
      resources :programs, only: [:index]
    end
  end

  namespace :programs do
    get :popular, to: 'popular'
  end

  resources :programs, only: [:index], defaults: { format: :json } do
    member do
      post :subscribe, to: 'programs#subscribe'
    end
  end
end
