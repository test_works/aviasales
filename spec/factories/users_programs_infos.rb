FactoryBot.define do
  factory :users_programs_info do
    ban { false }

    user
    program
  end
end
