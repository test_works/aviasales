FactoryBot.define do
  factory :program do
    title { Faker::Lorem.word }
    description { Faker::Lorem.sentence }
  end
end
