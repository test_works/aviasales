module Request
  module AuthHelpers
    def auth_headers(user)
      token = user.set_new_token
      {
        'Authorization': "Bearer #{token}",
        'Content-Type': 'application/json'
      }
    end

    def authenticated_header(admin)
      token = admin.set_new_token
      { 'Authorization': "Bearer #{token}" }
    end

    def auth_request!(request, admin)
      request.headers['accept'] = 'application/json'
      request.headers.merge! authenticated_header(admin)
    end
  end
end
