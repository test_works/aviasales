RSpec.describe Program, type: :request do
  let(:user) { create(:user) }
  let(:program) { create(:program) }

  describe 'GET #index' do
    context 'with valid attributes' do
      before(:each) { get user_programs_path(user) }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('programs') }
    end

    context 'with invalid attributes' do
      before(:each) { get user_programs_path(0) }
      it { expect(response).to have_http_status(:not_found) }
    end
  end
end
