RSpec.describe Program, type: :request do
  let(:admin) { create(:admin) }
  let(:user) { create(:user) }
  let(:program) { create(:program) }

  describe 'PUT #ban' do
    context 'with valid attributes' do
      before(:each) { put ban_admins_program_path(program), params: { email: user.email }, headers: auth_headers(admin), as: :json }
      it { expect(response).to have_http_status(:ok) }
    end

    context 'with invalid attributes' do
      it do
        put ban_admins_program_path(program), params: { email: '' }, headers: auth_headers(admin), as: :json
        expect(response).to have_http_status(:not_found)
      end

      it do
        put ban_admins_program_path(program), headers: auth_headers(admin), as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'with invalid headers' do
      before(:each) { put ban_admins_program_path(program), params: { email: user.email }, as: :json }
      it { expect(response).to have_http_status(:unauthorized) }
    end
  end
end
