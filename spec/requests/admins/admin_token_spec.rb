RSpec.describe Admin, type: :request do
  let(:admin) { create(:admin) }

  describe 'POST #create' do
    context 'with valid attributes' do
      before(:each) { post admins_sessions_path, params: { email: admin.email, password: admin.password }, as: :json }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('admins/admins') }
    end

    context 'with invalid attributes' do
      before(:each) { post admins_sessions_path, params: { email: '', password: admin.password } }
      it { expect(response).to have_http_status(:not_found) }
    end
  end
end
