RSpec.describe User, type: :request do
  let(:user) { create(:user) }

  describe 'GET #show' do
    context 'with valid attributes' do
      before(:each) { get user_path(user) }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('users') }
    end

    context 'with invalid attributes' do
      before(:each) { get user_path(0) }
      it { expect(response).to have_http_status(:not_found) }
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      before(:each) { post users_path, params: attributes_for(:user), as: :json }
      it { expect(response).to have_http_status(:created) }
      it { expect(response).to match_response_schema('users') }
    end

    context 'with invalid attributes' do
      before(:each) { post users_path, params: attributes_for(:user, password: '') }
      it { expect(response).to have_http_status(:unprocessable_entity) }
    end
  end
end
