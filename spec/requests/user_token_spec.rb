RSpec.describe User, type: :request do
  let(:user) { create(:user) }

  describe 'POST #create' do
    context 'with valid attributes' do
      before(:each) { post sessions_path, params: { email: user.email, password: user.password }, as: :json }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('users') }
    end

    context 'with invalid attributes' do
      before(:each) { post sessions_path, params: { email: '', password: user.password } }
      it { expect(response).to have_http_status(:not_found) }
    end
  end
end
