RSpec.describe Program, type: :request do
  let(:user) { create(:user) }
  let(:program) { create(:program) }

  describe 'GET #index' do
    context 'with valid attributes' do
      before(:each) { get programs_path }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('programs') }
    end
  end

  describe 'POST #subscribe' do
    context 'with valid attributes' do
      before(:each) { post subscribe_program_path(program), headers: auth_headers(user), as: :json }
      it { expect(response).to have_http_status(:ok) }
    end

    context 'with invalid attributes' do
      before(:each) { post subscribe_program_path(0), headers: auth_headers(user), as: :json }
      it { expect(response).to have_http_status(:not_found) }
    end

    context 'with invalid headers' do
      before(:each) { post subscribe_program_path(program), as: :json }
      it { expect(response).to have_http_status(:unauthorized) }
    end
  end

  describe 'GET #popular' do
    context 'with valid attributes' do
      before(:each) { get programs_popular_path }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('programs') }
    end
  end
end
