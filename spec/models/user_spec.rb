RSpec.describe User, type: :model do
  it { expect(build(:user)).to be_valid }

  let!(:described_class_object) { create(:user) }

  describe 'ActiveModel' do
    context 'associations' do
      it { expect(described_class_object).to have_many(:users_programs_infos).dependent(:destroy) }
      it { expect(described_class_object).to have_many(:programs).through(:users_programs_infos) }
      it { expect(described_class_object).to have_many(:available_programs).through(:users_programs_infos).source(:program) }
    end

    context 'callbacks' do
      it { expect(described_class_object).to callback(:normalize_name).before(:validation) }
    end

    context 'validations' do
      it { expect(described_class_object).to validate_presence_of(:password_digest) }
      it { expect(described_class_object).to validate_presence_of(:name) }
      it { expect(described_class_object).to allow_value('_success@rspec.test_').for(:email) }
      it { expect(described_class_object).not_to allow_value('failed@rspec_test.').for(:email) }
      it { expect(described_class_object).not_to allow_value('failed@rspec_test').for(:email) }
      it { expect(described_class_object).not_to allow_value('failed_rspec@.test').for(:email) }
      it { expect(described_class_object).not_to allow_value('@failed_rspec.test').for(:email) }
      it { expect(described_class_object).not_to allow_value('failed_rspec.test').for(:email) }
    end
  end
end
