RSpec.describe UsersProgramsInfo, type: :model do
  it { expect(build(:users_programs_info)).to be_valid }

  let!(:described_class_object) { create(:users_programs_info) }

  describe 'ActiveModel' do
    context 'associations' do
      it { expect(described_class_object).to belong_to(:user) }
      it { expect(described_class_object).to belong_to(:program) }
    end
  end
end
