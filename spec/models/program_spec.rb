RSpec.describe Program, type: :model do
  it { expect(build(:program)).to be_valid }

  let!(:described_class_object) { create(:program) }

  describe 'ActiveModel' do
    context 'associations' do
      it { expect(described_class_object).to have_many(:users_programs_infos).dependent(:destroy) }
      it { expect(described_class_object).to have_many(:users).through(:users_programs_infos) }
    end

    context 'callbacks' do
      it { expect(described_class_object).to callback(:normalize_title).before(:validation) }
      it { expect(described_class_object).to callback(:normalize_description).before(:validation) }
    end

    context 'scopes' do
      it { expect(described_class).to respond_to(:term)}
      it { expect(described_class).to respond_to(:by_popularity)}
    end

    context 'validations' do
      it { expect(described_class_object).to validate_presence_of(:title) }
      it { expect(described_class_object).to validate_length_of(:title).is_at_least(1).is_at_most(30) }
      it { expect(described_class_object).to validate_presence_of(:description) }
      it { expect(described_class_object).to validate_length_of(:description).is_at_least(1).is_at_most(140) }
    end
  end
end
