RSpec.describe Admin, type: :model do
  it { expect(build(:admin)).to be_valid }

  let!(:described_class_object) { create(:admin) }

  describe 'ActiveModel' do
    context 'validations' do
      it { expect(described_class_object).to validate_presence_of(:password_digest) }
      it { expect(described_class_object).to allow_value('_success@rspec.test_').for(:email) }
      it { expect(described_class_object).not_to allow_value('failed@rspec_test.').for(:email) }
      it { expect(described_class_object).not_to allow_value('failed@rspec_test').for(:email) }
      it { expect(described_class_object).not_to allow_value('failed_rspec@.test').for(:email) }
      it { expect(described_class_object).not_to allow_value('@failed_rspec.test').for(:email) }
      it { expect(described_class_object).not_to allow_value('failed_rspec.test').for(:email) }
    end
  end
end
